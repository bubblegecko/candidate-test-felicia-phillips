var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () {

    var col = [];
    var table = document.createElement("table");
    var tr;

    if (this.readyState == 4 && this.status == 200) {
        var actresses = JSON.parse(xhttp.responseText);
        var col = [];

        // Creating table element
        var table = document.createElement("table");
        tr = table.insertRow(-1);

        // Column Header Count
        for (var key in actresses[0]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
                console.log(col);
            }
        }

        // Column Header Output
        for (var i = 0; i < col.length; i++) {
            var th = document.createElement("th");
            th.innerHTML = col[i];
            tr.appendChild(th);
        }
        // Going through the length of the json object
        for (var i = 1; i < actresses.length; i++) {
            tr = table.insertRow(-1);
            // adding the information from the rest of the file into respective columns and
            // rows.
            for (var j = 0; j < col.length; j++) {
                var tabCell = tr.insertCell(-1);
                tabCell.innerHTML = actresses[i][col[j]];
            }

        }

        // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
        var divContainer = document.getElementById("output");
        divContainer.innerHTML = "";
        divContainer.appendChild(table);

    }
};
xhttp.open("GET", "http://127.0.0.1:8000/output.json", true);
xhttp.send();
